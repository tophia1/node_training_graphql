import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentManagerResolver } from './department-manager.resolver';
import { DepartmentManagerService } from './department-manager.service';

describe('DepartmentManagerResolver', () => {
  let resolver: DepartmentManagerResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DepartmentManagerResolver, DepartmentManagerService],
    }).compile();

    resolver = module.get<DepartmentManagerResolver>(DepartmentManagerResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
