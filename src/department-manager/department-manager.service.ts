import { Inject, Injectable } from '@nestjs/common';
import { DEPARTMENT_MANAGER_REPOSITORY } from 'src/constants';
import { EmployeeService } from 'src/employee/employee.service';
import { Department_Manager } from './department-manager.model';
import { ManagerCreateInputDto } from './dto/create-department-manager.dto';

@Injectable()
export class DepartmentManagerService {
  constructor(
    private readonly employeeService: EmployeeService,
    @Inject(DEPARTMENT_MANAGER_REPOSITORY)
    private readonly departmentManagerRepository: typeof Department_Manager,
  ) {}
    // department manager updation with department_id and employee_id
  async deptManagerCreate(managerCreateInput: ManagerCreateInputDto) {
    try {
      // find whether department is assigned for corresponding employee
      const findEmp =
        await this.departmentManagerRepository.findOne<Department_Manager>({
          where: {
            employee_id: managerCreateInput.employee_id,
          },
        });
        // if employee is not assigned create an entry
      if (findEmp == null) {
        return await this.departmentManagerRepository.create<Department_Manager>(
          {
            employee_id: managerCreateInput.employee_id,
            department_id: managerCreateInput.department_id,
            roles: 'Manager',
          },
        );
      }
      // else updating the existed entry
      const data = {
        employee_id: managerCreateInput.employee_id,
        department_id: managerCreateInput.department_id,
        roles: 'Manager',
      };
      // updating role of employee in employee table
      const updateEmployeeDetails = await this.employeeService.managerUpdate(
        managerCreateInput.employee_id,
      );

      return await this.departmentManagerRepository.update<Department_Manager>(
        data,
        {
          where: {
            employee_id: managerCreateInput.employee_id,
          },
        },
      );
    } catch (error) {
      throw error;
    }
  }
}
