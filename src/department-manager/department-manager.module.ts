import { Module } from '@nestjs/common';
import { DepartmentManagerService } from './department-manager.service';
import { DepartmentManagerResolver } from './department-manager.resolver';
import { SequelizeModule } from '@nestjs/sequelize';
import { Department_Manager } from './department-manager.model';
import { departmentManagerProviders } from './department-manager.providers';
import { EmployeeModule } from 'src/employee/employee.module';

@Module({
  imports: [SequelizeModule.forFeature([Department_Manager]), EmployeeModule],

  providers: [
    DepartmentManagerResolver,
    DepartmentManagerService,
    ...departmentManagerProviders,
  ],
})
export class DepartmentManagerModule {}
