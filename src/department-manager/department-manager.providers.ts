import { DEPARTMENT_MANAGER_REPOSITORY } from 'src/constants';
import { Department_Manager } from './department-manager.model';

export const departmentManagerProviders = [
  {
    provide: DEPARTMENT_MANAGER_REPOSITORY,
    useValue: Department_Manager,
  },
];
