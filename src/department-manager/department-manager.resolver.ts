import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { DepartmentManagerService } from './department-manager.service';
import { ManagerCreateInputDto } from './dto/create-department-manager.dto';
import { DepartmentManagerDto } from './dto/department-manager.dto';

@Resolver()
export class DepartmentManagerResolver {
  constructor(
    private readonly departmentManagerService: DepartmentManagerService,
  ) {}
  // department manager updation with department_id and employee_id
  @Mutation(() => [DepartmentManagerDto], {
    name: 'departmentManagerUpdation',
    nullable: true,
    description: 'Department Manager Updation',
  })
  async departmentManagerUpdation(
    @Args('managerCreateInput') managerCreateInput: ManagerCreateInputDto,
  ) {
    return await this.departmentManagerService.deptManagerCreate(
      managerCreateInput,
    );
  }
}
