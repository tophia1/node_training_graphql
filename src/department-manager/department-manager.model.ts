/* eslint-disable prettier/prettier */
import { Column, Model, Table, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Department } from 'src/department/department.model';
import { Employee } from 'src/employee/employee.model';

@Table
export class Department_Manager extends Model {
  @ForeignKey(() => Employee)
  @Column({
    type: DataType.BIGINT,
})
employee_id:number

@ForeignKey(() => Department)
@Column({
  type: DataType.BIGINT,
})
  department_id: number;

  @Column({
    type: DataType.DATE,
    defaultValue: Date.now(),
    allowNull: false,
  })
  from_date:Date;

  @Column({
    type: DataType.DATE,
    defaultValue: Date.now(),
    allowNull: false,
  }) 
   to_date:Date;
   
   @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
    allowNull: false,
  })
  deleted: boolean;
  @BelongsTo(() => Employee)
  employee: Employee
  @BelongsTo(() => Department)
  department: Department
}
