import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentManagerService } from './department-manager.service';

describe('DepartmentManagerService', () => {
  let service: DepartmentManagerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DepartmentManagerService],
    }).compile();

    service = module.get<DepartmentManagerService>(DepartmentManagerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
