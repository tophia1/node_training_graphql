/* eslint-disable prettier/prettier */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Role } from 'src/constants/role.enum';

@ObjectType()
export class DepartmentManagerDto {
  @Field(() => Int, { nullable: true }) id: number;
  @Field(() => Int, { nullable: true }) employee_id: number;
  @Field(() => Int, { nullable: true }) department_id: number;
  @Field(() => String, { nullable: true }) roles: Role;
  @Field({ nullable: false }) createdAt: Date;
  @Field({ nullable: false }) updatedAt: Date;
}
