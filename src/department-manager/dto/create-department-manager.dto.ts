/* eslint-disable prettier/prettier */
import { InputType, Field, Int } from '@nestjs/graphql';
import { Role } from 'src/constants/role.enum';

@InputType()
export class ManagerCreateInputDto {
  @Field(() => Int, { nullable: true }) employee_id: number;
  @Field(() => Int, { nullable: true }) department_id: number;
}
