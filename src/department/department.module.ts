/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { DepartmentService } from './department.service';
import { DepartmentResolver } from './department.resolver';
import { Department } from './department.model';
import { SequelizeModule } from '@nestjs/sequelize';
import { departmentProviders } from './department.providers';

@Module({
  imports:[
    SequelizeModule.forFeature([Department])

  ],
  providers: [DepartmentResolver, DepartmentService, ...departmentProviders]
})
export class DepartmentModule {}
