import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class CreateDepartmentInput {
  @Field(() => String, { nullable: true }) dept_name: string;
  // @Field(() => String, { nullable: true }) deleted: string;
}
@InputType()
export class UpdateDepartmentInput {
  @Field(() => Int, { nullable: true }) id: number;
  @Field(() => String, { nullable: true }) dept_name: string;
  @Field(() => String, { nullable: true }) deleted: string;
}
@InputType()
export class DeleteDepartmentInput {
  @Field(() => Int, { nullable: true }) id: number;
}
