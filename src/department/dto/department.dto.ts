/* eslint-disable prettier/prettier */
import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class DepartmentDto {
  @Field(() => Int, { nullable: true }) id: number;
  @Field(() => String, { nullable: true }) dept_name: string;
  @Field(() => String, { nullable: true }) deleted: string;
  @Field({ nullable: false }) createdAt: Date;
  @Field({ nullable: false }) updatedAt: Date;
}
