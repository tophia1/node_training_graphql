/* eslint-disable prettier/prettier */

import { DEPARTMENT_REPOSITORY } from "src/constants";
import { Department } from "./department.model";

export const departmentProviders = [
    {
        provide: DEPARTMENT_REPOSITORY,
        useValue: Department,
    },
];
