/* eslint-disable prettier/prettier */
import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { DepartmentService } from './department.service';
import { Department } from './department.model';
import { CreateDepartmentInput, DeleteDepartmentInput, UpdateDepartmentInput } from './dto/create-department.input';
import { DepartmentDto } from './dto/department.dto';

@Resolver(() => Department)
export class DepartmentResolver {
  constructor(private readonly departmentService: DepartmentService) {}
// creation of department
  @Mutation(() => DepartmentDto,{ 
    name: 'departmentCreate' ,
    nullable: true,
    description: 'Create Department',
  })
  async createDepartment(@Args('createDepartmentInput') createDepartmentInput: CreateDepartmentInput) {
    return await this.departmentService.create(createDepartmentInput);
  }
  // update department 
  @Mutation(() => DepartmentDto,{ 
    name: 'departmentUpdate' ,
    nullable: true,
    description: 'Update Department',
  })
  async updateDepartment(@Args('updateDepartmentInput') updateDepartmentInput: UpdateDepartmentInput) {
    return await this.departmentService.update(updateDepartmentInput);
  }
  // fetch all departments
  @Query(() => [DepartmentDto], 
  { name: 'departmentList',
  nullable: true,
  description: 'List all Departments'
})
  async findAll() {
    return this.departmentService.findAll();
  }
  // find department id
  @Query(() => DepartmentDto, { name: 'departmentById' })
  findOne(@Args('id') id: number) {
    return this.departmentService.findOne(id);
  }
// soft delete by id
  @Mutation(() => DepartmentDto, { name: 'deleteById',
  nullable: true,
  description: 'Delete Department by id' })
  async removeDepartment(@Args('deleteDepartmentDto') deleteDepartmentDto: DeleteDepartmentInput ) {

    return await this.departmentService.remove(deleteDepartmentDto);
  }
}
