/* eslint-disable @typescript-eslint/no-unused-vars */
import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { DEPARTMENT_REPOSITORY } from 'src/constants';
import { Department } from './department.model';
import {
  CreateDepartmentInput,
  DeleteDepartmentInput,
  UpdateDepartmentInput,
} from './dto/create-department.input';

@Injectable()
export class DepartmentService {
  constructor(
    @Inject(DEPARTMENT_REPOSITORY)
    private readonly departmentRepository: typeof Department,
  ) {}
  // create department
  async create(createDepartmentInput: CreateDepartmentInput) {
    try {
      return await this.departmentRepository.create<Department>({
        dept_name: createDepartmentInput.dept_name,
        deleted: false,
      });
    } catch (error) {
      throw error;
    }
  }
  // update department
  async update(updateDepartmentInput: UpdateDepartmentInput) {
    try {
      return await this.departmentRepository.update<Department>(
        {
          dept_name: updateDepartmentInput.dept_name,
          deleted: false,
        },
        {
          where: { id: updateDepartmentInput.id },
          returning: true,
        },
      );
    } catch (error) {
      throw error;
    }
  }
  // all departments
  async findAll() {
    try {
      return await this.departmentRepository.findAll<Department>();
    } catch (error) {
      throw new BadRequestException();
    }
  }
  // find department by id
  async findOne(id: number) {
    try {
      return await this.departmentRepository.findOne<Department>({
        where: {
          id: id,
        },
      });
    } catch (error) {
      throw new BadRequestException();
    }
  }
  // soft delete by id
  async remove(deleteDepartmentDto: DeleteDepartmentInput) {
    try {
      return await this.departmentRepository.update<Department>(
        { deleted: true }, // set de;eted flag to true
        {
          where: {
            id: deleteDepartmentDto.id,
          },
          returning: true,
        },
      );
    } catch (error) {
      throw error;
    }
  }
}
