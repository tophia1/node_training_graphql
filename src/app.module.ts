/* eslint-disable prettier/prettier */
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// import { EmployeesModule } from './employees/employees.module';
import { SequelizeModule } from '@nestjs/sequelize';
import { EmployeeModule } from './employee/employee.module';
import { DepartmentModule } from './department/department.module';
import { SalaryModule } from './salary/salary.module';
import { DepartmentManagerModule } from './department-manager/department-manager.module';
import { EmployeeProfileModule } from './employee-profile/employee-profile.module';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([{ name: 'SALARY_MICROSERVICE', transport: Transport.TCP }]),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'password',
      database: 'filter',
      autoLoadModels: true,
      synchronize: true,
    }),
    EmployeeModule,
    DepartmentModule,
    SalaryModule,
    DepartmentManagerModule,
    EmployeeProfileModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
