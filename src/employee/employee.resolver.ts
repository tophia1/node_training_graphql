/* eslint-disable prettier/prettier */
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { EmployeeCreateInputDto, EmployeeUpdateInputDto, RoleUpdateInputDto } from './dto/create-employee.dto';
import { EmployeeListArgsDto } from './dto/employee-list.args.dto';
import { EmployeeDto } from './dto/employee.dto';
import { EmployeeService } from './employee.service';

@Resolver()
export class EmployeeResolver {
  constructor(private readonly employeeService: EmployeeService) {}
  // creation of employee
  @Mutation(() => EmployeeDto, { 
    name: 'employeeCreate' ,
    nullable: true,
    description: 'Create Employee',
  })
  async employeeCreate(@Args('employeeCreateInput') employeeCreateInput: EmployeeCreateInputDto) {
    return await this.employeeService.employeeCreate(employeeCreateInput);
  
  }
  // fetch all employees details
  @Query(() => [EmployeeDto], {
    name: 'employeeList',
    nullable: true,
    description: 'List of employees',
  })
  async employeeList() {
    return this.employeeService.employeeList();
  }
  // update employee details
  @Mutation(() => [EmployeeDto], { 
    name: 'employeeUpdate' ,
    nullable: true,
    description: 'Update Employee',
  })
  async employeeUpdate(@Args('employeeUpdateInput') employeeUpdateInput: EmployeeUpdateInputDto) {
    return await this.employeeService.employeeUpdate(employeeUpdateInput);
  }
  // update role
  @Mutation(() => [EmployeeDto], { 
    name: 'employeeRoleUpdate' ,
    nullable: true,
    description: 'Update Employee Role',
  })
  async roleUpdate(@Args('roleUpdateInput') roleUpdateInput: RoleUpdateInputDto) {
    return await this.employeeService.roleUpdate(roleUpdateInput);
  }
  // find employee by id
  @Query(() => EmployeeDto, { name: 'employeeById' })
  findOne(@Args('id') id: number) {
    return this.employeeService.findOne(id);
  }

  @Query(() => [EmployeeDto], {
    name: 'employeeFilter',
    nullable: true,
    description: 'filter for employee details',
  })
  async employeeFilter(@Args() { textFilter, page, paging }: EmployeeListArgsDto) {
    return this.employeeService.employeeFilter(textFilter, page, paging);
  }
}
