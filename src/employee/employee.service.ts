/* eslint-disable prettier/prettier */
import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { Employee } from './employee.model';
import * as bcrypt from 'bcrypt';
import { isEmpty } from 'lodash';
import { EMPLOYEE_REPOSITORY } from '../constants/index'
import { EmployeeCreateInputDto, RoleUpdateInputDto } from './dto/create-employee.dto';
import { Role } from 'src/constants/role.enum';
import { Op } from 'sequelize';

@Injectable()
export class EmployeeService {
    constructor(
        @Inject(EMPLOYEE_REPOSITORY) private readonly employeeRepository: typeof Employee,
      ) {}
      // private readonly includedModels = [Department];

      async employeeCreate(employeeCreateInput: EmployeeCreateInputDto): Promise<Employee> {
        try {
          const hash: string = await EmployeeService.hashPassword(
            employeeCreateInput.password,
            12,
          );
          return await this.employeeRepository.create<Employee>(
            {
            first_name: employeeCreateInput.first_name,
            last_name: employeeCreateInput.last_name,
            email : employeeCreateInput.email,
            password: hash,
            roles: employeeCreateInput.roles,
            employee_gender: employeeCreateInput.employee_gender
          }
          );
        } catch (error) {
          throw error
      }
    }
      //hash password
      static async hashPassword(password: string, rounds: number): Promise<string> {
        return await bcrypt.hash(password, rounds);
      }
    // list of all employees
    async employeeList(): Promise<Employee[]> {
        try {
 
          return await this.employeeRepository.findAll<Employee>();
        } catch (error) {
          throw new BadRequestException();
        }
      }
    //update employee details from employee-profile module
    async employeeUpdate(updateProfile) {
      try {
        const pass = await EmployeeService.hashPassword(updateProfile.password, 12);
        const data = {
            first_name: updateProfile.first_name,
            last_name: updateProfile.last_name,
            email: updateProfile.email,
            password: pass,
            employee_gender: updateProfile.employee_gender
        }
        
        const [numberOfAffectedRows, [updatedPost]] = await this.employeeRepository.update(data, { where: { id : updateProfile.employee_id }, returning: true });
        return { numberOfAffectedRows, updatedPost };
      }
      catch(error){
        return error
      }
  } 
  // update role
  async roleUpdate(roleUpdateInput: RoleUpdateInputDto) {
    try {
     
      const data = {
          roles: roleUpdateInput.roles,
    }    
    const [numberOfAffectedRows, [updatedPost]] = await this.employeeRepository.update(data, { where: { id : roleUpdateInput.id }, returning: true });
    return { numberOfAffectedRows, updatedPost };
    } catch (error) {
      throw new BadRequestException();
  }
}
// manager role update from department-manager module
async managerUpdate(employee_id) {
  try {
   
    const data = {
        roles: Role.MANAGER,
  }    
  const [numberOfAffectedRows, [updatedPost]] = await this.employeeRepository.update(data, { where: { id : employee_id }, returning: true });
  return { numberOfAffectedRows, updatedPost };
  } catch (error) {
    throw new BadRequestException();
}
}
  // find employee by id

  async findOne(id: number) {
    try {
      return await this.employeeRepository.findOne<Employee>({
        where: {
          id: id,
        },
      });
    } catch (error) {
      throw new BadRequestException();
    }
  }
  // filter
  async employeeFilter(
    textFilter: string,
    page: number,
    paging: number,
  ): Promise<Employee[] | undefined> {
    try {
      const iRegexp: string = isEmpty(textFilter)
        ? ``
        : `(^${textFilter})|( ${textFilter})`;
      return await this.employeeRepository.findAll<Employee>({
        limit: paging,
        offset: (page - 1) * paging,
        // include: this.includedModels,
        where: {
          first_name: {
            [Op.iRegexp]: iRegexp,
          },
          last_name: {
            [Op.iRegexp]: iRegexp,
          },
          roles: {
            [Op.iRegexp]: iRegexp,
          },
        },
        order: [['first_name', 'ASC']],
      });
    } catch (error) {
      throw error;
    }
  }
}
