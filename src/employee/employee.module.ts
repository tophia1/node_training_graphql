/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { EmployeeResolver } from './employee.resolver';
import { Employee } from './employee.model';
import { SequelizeModule } from '@nestjs/sequelize';
import { employeeProviders } from './employee.providers';

@Module({
  imports:[
    SequelizeModule.forFeature([Employee])

  ],
  exports:[EmployeeService],
  providers: [EmployeeResolver, EmployeeService, ...employeeProviders]
})
export class EmployeeModule {}
