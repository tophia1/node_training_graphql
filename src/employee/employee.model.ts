/* eslint-disable prettier/prettier */
import { Table, Column, Model, DataType, HasMany } from 'sequelize-typescript';
import { Gender } from 'src/constants/gender.enum';
import { Department } from 'src/department/department.model';
import { Role } from '../constants/role.enum';
// import { Department_Manager } from './departmentManager';
// import { Employee_Profile } from './empolyeeProfile.model';
// import { Salary } from './salary.model';
// import { Title } from './title.model';

@Table
export class Employee extends Model<Employee> {
  @Column({
    type: DataType.BIGINT,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
  })
  public id: number;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  first_name: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  last_name: string;

  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  email: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  password: string;
  @Column({
    type: DataType.STRING,
    allowNull: false,
    defaultValue: Role.EMPLOYEE
  })
  roles: Role;

  @Column({
    type: DataType.DATE,
    allowNull: false,
    defaultValue: '2022-04-05 14:08:25.979+05:30',

  })
  birth_date: Date;

  @Column({
    type: DataType.DATE,
    allowNull: false,
    defaultValue: '2022-04-05 14:08:25.979+05:30',

  })
  hire_date: Date;

  @Column({
      type: DataType.ENUM,
      values: ['M', 'F'],
      allowNull: true,
  })
  employee_gender: Gender;
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
    allowNull: false,
  })
  deleted: boolean;
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
    allowNull: false,
  })
  removed: boolean;
  //   @HasMany(() => Department)
  // department: Department[]
//   @HasMany(() => Salary)
//   salary: Salary[]
//   @HasMany(() => Title)
//   title: Title[]
//   @HasMany(() => Department_Manager)
//   departmentManager: Department_Manager[]
//   @HasMany(() => Employee_Profile)
//   empolyeeProfile: Employee_Profile[]
}

