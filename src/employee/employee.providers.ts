/* eslint-disable prettier/prettier */

import { EMPLOYEE_REPOSITORY } from "src/constants";
import { Employee } from "./employee.model";

export const employeeProviders = [
    {
        provide: EMPLOYEE_REPOSITORY,
        useValue: Employee,
    },
];
