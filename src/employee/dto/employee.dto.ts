/* eslint-disable prettier/prettier */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Role } from '../../constants/role.enum';

@ObjectType()
export class EmployeeDto {
  @Field(() => Int, { nullable: false }) id: number;
  @Field(() => String, { nullable: true }) first_name: string;
  @Field(() => String, { nullable: true }) last_name: string;
  @Field(() => String, { nullable: true }) email: string;
  @Field(() => String, { nullable: true }) employee_gender: string;
  @Field(() => String, { nullable: false }) roles: Role;
  @Field(() => Date, { nullable: true }) birth_date: Date;
  @Field(() => Date, { nullable: true }) hire_date: Date;
  //
    @Field(() => String, { nullable: true }) textFilter: string;
  @Field(() => Int, { nullable: false }) paging: number;
  @Field(() => Int, { nullable: false }) page: number;
  @Field({ nullable: false }) createdAt: Date;
  @Field({ nullable: false }) updatedAt: Date;
}
