/* eslint-disable prettier/prettier */
import { Field, InputType, Int } from '@nestjs/graphql';
import { Gender } from 'src/constants/gender.enum';
import { Role } from 'src/constants/role.enum';

@InputType()
export class EmployeeCreateInputDto {
  @Field(() => String, { nullable: true }) first_name: string;
  @Field(() => String, { nullable: true }) last_name: string;
  @Field(() => String, { nullable: true }) email: string;
  @Field(() => String, { nullable: false }) password: string;
  @Field(() => String, { nullable: true }) employee_gender: Gender;
  @Field(() => String, { nullable: true }) roles: Role;
}
@InputType()
export class EmployeeUpdateInputDto {
  @Field(() => Int, { nullable: false }) id: number;

  @Field(() => String, { nullable: false }) first_name: string;
  @Field(() => String, { nullable: false }) last_name: string;
  @Field(() => String, { nullable: false }) email: string;
  @Field(() => String, { nullable: false }) password: string;
  @Field(() => String, { nullable: false }) employee_gender: Gender;
  @Field(() => String, { nullable: false }) roles: Role;
}
@InputType()
export class RoleUpdateInputDto {
  @Field(() => Int, { nullable: true }) id: number;

  @Field(() => String, { nullable: true }) roles: Role;
}