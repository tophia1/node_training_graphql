import { Module } from '@nestjs/common';
import { SalaryService } from './salary.service';
import { SalaryResolver } from './salary.resolver';
import { Salary } from './salary.model';
import { SequelizeModule } from '@nestjs/sequelize';
import { salaryProviders } from './salary.providers';

@Module({
  imports: [SequelizeModule.forFeature([Salary])],
  providers: [SalaryResolver, SalaryService, ...salaryProviders],
})
export class SalaryModule {}
