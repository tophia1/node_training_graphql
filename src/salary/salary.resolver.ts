import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { SalaryCreateInputDto } from './dto/create-salary.dto';
import { SalaryDto } from './dto/salary.dto';
import { SalaryService } from './salary.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
@ApiTags('Salary')
@Resolver()
export class SalaryResolver {
  constructor(private readonly salaryService: SalaryService) {}

  @Mutation(() => [SalaryDto], {
    name: 'salaryUpdation',
    nullable: true,
    description: 'Update Salary',
  })
  async salaryUpdation(
    @Args('salaryCreateInput') salaryCreateInput: SalaryCreateInputDto,
  ) {
    return await this.salaryService.salaryCreate(salaryCreateInput);
  }
  @Query(() => [SalaryDto], {
    name: 'salaryList',
    nullable: true,
    description: 'List all salaries',
  })
  async findAll() {
    return this.salaryService.findAll();
  }
}
