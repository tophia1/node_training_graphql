/* eslint-disable prettier/prettier */
import { Inject, Injectable } from '@nestjs/common';
import { SALARY_REPOSITORY } from 'src/constants';
import { SalaryCreateInputDto, SalaryUpdateDto } from './dto/create-salary.dto';
import { Salary } from './salary.model';

@Injectable()
export class SalaryService {
  constructor(
    @Inject(SALARY_REPOSITORY)
    private readonly salaryRepository: typeof Salary,
  ) {}
  async salaryCreate(salaryCreateInput: SalaryCreateInputDto){
    try {
      const findEmp = await this.salaryRepository.findOne<Salary>({
        where: {
          employee_id: salaryCreateInput.employee_id,
        },
      });
      if (findEmp == null) {
        return await this.salaryRepository.create<Salary>({
          employee_id: salaryCreateInput.employee_id,
          amount: salaryCreateInput.amount,
        });
      }
      const data = {
        amount: salaryCreateInput.amount,
      };
      return  await this.salaryRepository.update<Salary>(data, {
        where: {
          employee_id: salaryCreateInput.employee_id,
        },
      });
    } catch (error) {
      throw error;
    }
  }
  async findAll() {
    try {
      return await this.salaryRepository.findAll<Salary>();
    } catch (error) {
      throw error;
    }
  }
  async salaryUpdate(employee_id:number,salaryUpdateInput: SalaryUpdateDto) {
    try {
      const data = {
          amount: salaryUpdateInput.amount,
        };
        return  await this.salaryRepository.update<Salary>(data, {
          where: {
            employee_id: employee_id,
          },
        });
    } catch (error) {
      throw error;
    }
  }
}
