import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import {
  ConfirmationInputDto,
  ProfileEditInputDto,
} from './dto/create-employee.dto';
import { EmployeeProfileDto } from './dto/employee-profile.dto';
import { EmployeeProfileService } from './employee-profile.service';

@Resolver()
export class EmployeeProfileResolver {
  constructor(
    private readonly employeeProfileService: EmployeeProfileService,
  ) {}
// Profile Updation by user
  @Mutation(() => [EmployeeProfileDto], {
    name: 'profileUpdation',
    nullable: true,
    description: 'Profile Updation',
  })
  async profileUpdation(
    @Args('profileEditInput') profileEditInput: ProfileEditInputDto,
  ) {
    return await this.employeeProfileService.profileUpdation(profileEditInput);
  }
  @Mutation(() => [EmployeeProfileDto], {
    name: 'profileUpdationConfirmation',
    nullable: true,
    description: 'Confirmation Profile Updation',
  })
  async confirmUpdation(
    @Args('confirmationInput') confirmationInput: ConfirmationInputDto,
  ) {
    return await this.employeeProfileService.confirmUpdation(confirmationInput);
  }
  @Query(() => [EmployeeProfileDto], {
    name: 'profileUpdatedList',
    nullable: true,
    description: 'List of employees',
  })
  async employeeProfileUpdatedList() {
    return this.employeeProfileService.employeeProfileUpdatedList();
  }
  // find department id
  // @Query(() => EmployeeProfileDto, { name: 'profileEditConfirmationById' })
  // confirmUpdation(@Args('id') id: number) {
  //   return this.employeeProfileService.confirmUpdation(id);
  // }
}
