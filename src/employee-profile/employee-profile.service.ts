import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { EMPLOYEE_PROFILE_REPOSITORY } from 'src/constants';
import { EmployeeService } from 'src/employee/employee.service';
import {
  ConfirmationInputDto,
  ProfileEditInputDto,
} from './dto/create-employee.dto';
import { Employee_Profile } from './employee-profile.model';

@Injectable()
export class EmployeeProfileService {
  constructor(
    private readonly employeeService: EmployeeService,
    @Inject(EMPLOYEE_PROFILE_REPOSITORY)
    private readonly employeeProfileRepository: typeof Employee_Profile,
  ) {}
  // Profile Updation by user , entry to employee-profile table

  async profileUpdation(profileEditInput: ProfileEditInputDto) {
    try {
      profileEditInput.confirmed = false;
      return await this.employeeProfileRepository.create<Employee_Profile>(
        profileEditInput,
      );
    } catch (error) {
      throw error;
    }
  }
  // fetch all updated profile pending for confirmation
  async employeeProfileUpdatedList(): Promise<Employee_Profile[]> {
    try {
      return await this.employeeProfileRepository.findAll<Employee_Profile>({
        where: { confirmed: false }, // initially confirmed set as false
      });
    } catch (error) {
      throw new BadRequestException();
    }
  }
  // confirmation of profile updation from manager
  async confirmUpdation(confirmationInput: ConfirmationInputDto) {
    try {
      // confirmed will be true if accepted and destroyed is false
      const data = {
        confirmed: confirmationInput.confirmed,
      };
      const [numberOfAffectedRows, [updatedPost]] =
        await this.employeeProfileRepository.update(data, {
          where: { employee_id: confirmationInput.employee_id },
          returning: true,
        });
      // fetching profile details
      const updateProfile = await this.employeeProfileRepository.findOne({
        where: { employee_id: confirmationInput.employee_id },
      });
      // updating editted profile in employee table
      if (updateProfile.confirmed === true) {
        const proupdate = await this.employeeService.employeeUpdate(
          updateProfile,
        );
        return { proupdate };
      } else {
        // destroying data if it is not confirmed by manager
        const notConfirmed = await this.employeeProfileRepository.destroy({
          where: { employee_id: confirmationInput.employee_id },
        });
        return { numberOfAffectedRows, updatedPost, notConfirmed };
      }
    } catch (error) {
      throw error;
    }
  }
}
