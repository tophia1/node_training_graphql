import { Module } from '@nestjs/common';
import { EmployeeProfileService } from './employee-profile.service';
import { EmployeeProfileResolver } from './employee-profile.resolver';
import { SequelizeModule } from '@nestjs/sequelize';
import { Employee_Profile } from './employee-profile.model';
import { employeeProfileProviders } from './employee-profile.providers';
import { EmployeeModule } from 'src/employee/employee.module';

@Module({
  imports: [SequelizeModule.forFeature([Employee_Profile]), EmployeeModule],
  providers: [
    EmployeeProfileResolver,
    EmployeeProfileService,
    ...employeeProfileProviders,
  ],
})
export class EmployeeProfileModule {}
