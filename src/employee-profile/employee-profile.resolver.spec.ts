import { Test, TestingModule } from '@nestjs/testing';
import { EmployeeProfileResolver } from './employee-profile.resolver';
import { EmployeeProfileService } from './employee-profile.service';

describe('EmployeeProfileResolver', () => {
  let resolver: EmployeeProfileResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmployeeProfileResolver, EmployeeProfileService],
    }).compile();

    resolver = module.get<EmployeeProfileResolver>(EmployeeProfileResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
