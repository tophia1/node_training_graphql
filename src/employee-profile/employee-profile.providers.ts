/* eslint-disable prettier/prettier */

import { EMPLOYEE_PROFILE_REPOSITORY } from "src/constants";
import { Employee_Profile } from "./employee-profile.model";


export const employeeProfileProviders = [
    {
        provide: EMPLOYEE_PROFILE_REPOSITORY,
        useValue: Employee_Profile,
    },
];
