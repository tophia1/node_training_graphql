/* eslint-disable prettier/prettier */
import { InputType, Field, Int } from '@nestjs/graphql';
import { Gender } from 'src/constants/gender.enum';
import { Role } from 'src/constants/role.enum';

@InputType()
export class ProfileEditInputDto {
  @Field(() => Int, { nullable: true}) employee_id : number;
  @Field(() => String, { nullable: false }) first_name: string;
  @Field(() => String, { nullable: false }) last_name: string;
  @Field(() => String, { nullable: false }) email: string;
  @Field(() => String, { nullable: false }) password: string;
  @Field(() => String, { nullable: false }) employee_gender: Gender;
  @Field(() => String, { nullable: false }) roles: Role;
  @Field(() => Boolean, { nullable: true }) confirmed: boolean;

}
@InputType()
export class ConfirmationInputDto {
  @Field(() => Int, { nullable: true }) employee_id: number;
  @Field(() => Boolean, { nullable: true }) confirmed: boolean;
}