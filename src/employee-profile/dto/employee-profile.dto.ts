/* eslint-disable prettier/prettier */
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Gender } from 'src/constants/gender.enum';
import { Role } from 'src/constants/role.enum';
import { IsNotEmpty } from 'class-validator';

@ObjectType()
export class EmployeeProfileDto {
  @IsNotEmpty()
  @Field(() => Int, { nullable: true}) employee_id : number;
  @IsNotEmpty()
  @Field(() => String, { nullable: false }) first_name: string;
  @IsNotEmpty()
  @Field(() => String, { nullable: false }) last_name: string;
  @IsNotEmpty()
  @Field(() => String, { nullable: false }) email: string;
  @IsNotEmpty()
  @Field(() => String, { nullable: false }) password: string;
  @IsNotEmpty()
  @Field(() => String, { nullable: false }) employee_gender: Gender;
  @IsNotEmpty()
  @Field(() => String, { nullable: false }) roles: Role;
  @IsNotEmpty()
  @Field(() => Boolean, { nullable: false }) confirmed: boolean;

}
